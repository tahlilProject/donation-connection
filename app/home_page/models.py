from django.db import models
from PIL import Image as Image_PIL
from user.models import User


class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    description = models.CharField( max_length=2000)
    numbers = models.IntegerField()
  
    image = models.ForeignKey( 'Image',on_delete=models.DO_NOTHING)
    owner = models.ForeignKey( 'charity.Charity', on_delete=models.CASCADE )
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse("product_detail", kwargs={"pk": self.pk})

class voluntray_work(models.Model):
    name = models.CharField( max_length=50)
    description = models.CharField( max_length=50)
    _type = models.CharField( max_length=50)
    owner = models.ForeignKey( 'charity.Charity', on_delete=models.CASCADE)
    address = models.CharField( max_length=50)
    volunteers = models.ForeignKey( 'user.User_Costomer', on_delete=models.DO_NOTHING,blank=True, null=True)
    image = models.ForeignKey( 'Image', on_delete=models.DO_NOTHING)

class Image(models.Model):
    image  = models.ImageField( upload_to='_pics')
    
    def save(self, *args, **kwargs):
        super( Image, self).save(*args, **kwargs)
        img = Image_PIL.open(self.image.path)
        output_size = (400, 400)
        img.thumbnail(output_size)
        img.save(self.image.path)