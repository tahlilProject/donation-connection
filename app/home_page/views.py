from django.shortcuts import render,redirect
from home_page.models import (Product, voluntray_work)
from django.contrib.auth import logout as auth_logout
from charity.models import Charity
from user.models import (User,User_Costomer,Cart)
from django.core.paginator import Paginator
# Create your views here.
def home(request):
    context = {
        'products':Product.objects.all()[:8],
        'voluntaryWorks':voluntray_work.objects.all()[:8],
    }
    return render(request, 'home_page/home.html',context,{'title': "home"} )

def charities_page(request):
    contact_list = Charity.objects.all()
    paginator = Paginator(contact_list, 4) # Show 25 contacts per page.
    if request.GET.get('page') is not None:
        page_number = request.GET.get('page')
    else:
        page_number =1 
    page_obj = paginator.get_page(page_number)
    context = {
        'charities': page_obj,
    }
    return render( request, 'home_page/charity_page.html', context )

def products(request):
    contact_list = Product.objects.all()
    paginator = Paginator(contact_list, 4) # Show 25 contacts per page.
    if request.GET.get('page') is not None:
        page_number = request.GET.get('page')
    else:
        page_number =1 
    page_obj = paginator.get_page(page_number)
    context = {
        'products': page_obj,
        'title' : 'Products'
    }
    print(context)
    return render(request, 'home_page/products.html', context )
def productsProfile(request, productId):
    product = Product.objects.get(id=productId)
    context = {
        'product': product,
    }
    return render(request, 'home_page/productProfile.html', context)
def VWProfile(request, VWId):
    VW = voluntray_work.objects.get(id=VWId)
    context = {
        'vw': VW,
    }
  
    return render(request, 'home_page/vwProfile.html', context)

def cart(request):
    user=User_Costomer.objects.get(user=request.user)
    context = {
        'carts': Cart.objects.filter(user=user)
    }
    return render(request, 'home_page/cart.html', context )
def buy(request,PId):
    product=Product.objects.get(id=PId)
    user=User_Costomer.objects.get(user=request.user)
    cart=Cart(products=product,user=user,price=0,status=1,isPayed=False)
    cart.save()
    return redirect('home-page')
def voluntary_work(request):
    contact_list = voluntray_work.objects.all()
    paginator = Paginator(contact_list, 4) # Show 25 contacts per page.
    if request.GET.get('page') is not None:
        page_number = request.GET.get('page')
    else:
        page_number =1 
    page_obj = paginator.get_page(page_number)
    context = {
        'voluntaryWorks':page_obj,
        'title' : 'voluntary_work'
    }
    print(context)
    return render(request, 'home_page/voluntary_works.html', context)

def logout(request):
    auth_logout(request)
    context={
        'title':'logout'
    }
    return render(request, 'home_page/logout.html', context )
def search(request):
    template = 'home_page/search.html'

    if request.method == 'POST':
        ID = request.POST.get("Search...", "")
        users = User.objects.filter(Is_charity=True)
        users2 = []
        for user in users:
            if ID in user.fullName:
                users2.append(user)

        charity = []
        for user1 in users2:
            charity.append(Charity.objects.get( user = user1 ))
        prodocts = Product.objects.all()
        prodoctsFind = []
        for prodoct in prodocts:
            if ID in prodoct.name:
                prodoctsFind.append(prodoct)
        VWS=voluntray_work.objects.all()
        VWFind =[]
        for VW in VWS:
            if ID in VW.name:
                VWFind.append(VW)
        if charity is not None:
            context = {
                'voluntaryWorks':VWFind,
                'products':prodoctsFind,
                'users': charity,
                'title': 'search'
            }
            print (charity)
            return render(request, template, context)
        else:
            context = {
                'user': '',
                'title': 'search'
            }
            return render(request, template, context, {
                'error_message': 'user not find'
            })
    return render(request, template, {'title': "search"})

def about(request):
    return render(request, 'home_page/about.html')

def comingSoon(request):
    return render(request, 'home_page/comingSoon.html')
