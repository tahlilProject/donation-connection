from . import views
from django.urls import path

urlpatterns = [
    path('', views.home, name='home-page'),
    path('products/', views.products, name='products-page'),
    path('voluntary_work/', views.voluntary_work, name='voluntary_work-page'),
    path('search/', views.search, name='search'),
    path('charity_page/', views.charities_page, name='all-charity'),
    path('productsProfilePage/<int:productId>/', views.productsProfile, name="productsProfilePage"),
    path('VWProfilePage/<int:VWId>/', views.VWProfile, name="VWProfilePage"),
    path('about/', views.about, name="about"),
    path('comingSoon/', views.comingSoon, name="comingSoon"),
    path('cart/', views.cart, name="seeCart"),
    path('addCart/<int:PId>/', views.buy, name="addCart"),   
]