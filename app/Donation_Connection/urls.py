"""Donation_Connection URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path,include
from home_page import views as home_page_views
from django.conf import settings
from django.conf.urls.static import static
from user import views as user_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('charity/', include('charity.urls')),
    path('user_login/', user_views.User_login, name='user_login-page'),
    path('logout/', home_page_views.logout, name='logout'),
    path('user_register/', user_views.User_register, name='user_register-page'),
    path('user_costomer_profile/',user_views.User_Profile, name='user-costomer-profile' ),
    path('customerEditPage/', user_views.customerEditPage, name="customerEditPage"),
    path('customerSaveChanges/', user_views.customerSaveChanges, name='customerSaveChanges'),
    path('userProfileVW/<int:userId>/', user_views.userProfileVW, name="userProfileVW"),
    path('', include('home_page.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)