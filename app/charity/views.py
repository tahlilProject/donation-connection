from django.shortcuts import render,redirect
from .models import Charity
from user.models import (User,User_Costomer)
from home_page.models import (Product, voluntray_work, Image)
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.conf import settings
from django.core.files.storage import FileSystemStorage


def Charity_register(request):
    template = 'charity/charityRegister.html'
    if request.method == 'POST' and request.FILES.get('profileImage'):
        charity_description = request.POST.get("Description","")
        charity_username = request.POST.get("Username", "")
        charity_fullname =  request.POST.get("CharityName","")
        charity_email = request.POST.get("Email", "")
        charity_owner_name = request.POST.get("OwnerName", "")
        charity_address = request.POST.get("Location", "")
        charity_phone_number = request.POST.get("Phone", "")
        charity_AcquisitionLicenseCode = request.POST.get("AcquisitionLicenseCode", "")
        charity_password = request.POST.get("Password", "")
        charity_profile = request.FILES.get('profileImage')
        if User.objects.filter( username = charity_username ).exists():
            return render(request, template, {
                'error_message': 'Username already exists.'
            })
        elif User.objects.filter( fullName = charity_fullname ).exists():
            return render(request, template, {
                'error_message': 'CharityName already exists.'
            })
        elif User.objects.filter( email = charity_email ).exists():
            return render(request, template, {
                'error_message': 'Email already exists.'
            })
        elif request.POST.get("Password", "") !=  request.POST.get("Confirm-Password", ""):
            return render(request, template, {
                'error_message': 'Passwords not match.'
            })
        elif Charity.objects.filter( serialNumber = charity_AcquisitionLicenseCode ).exists():
            return render(request, template, {
                'error_message': 'AcquisitionLicenseCode already exists.'
            })
        elif User.objects.filter( phoneNumber = charity_phone_number ).exists():
            return render(request, template, {
                'error_message': 'phoneNumber already exists.'
            })
        else:
            abstract_user = User(description=charity_description,Is_charity= True, IsUser= False, username=charity_username , email = charity_email, phoneNumber =charity_phone_number , fullName= charity_fullname, ProfileImage = charity_profile)
            abstract_user.save()
            abstract_user.set_password(charity_password)
            abstract_user.save()
            charity = Charity(user=abstract_user,serialNumber=charity_AcquisitionLicenseCode,
                            Address= charity_address, ownerName=charity_owner_name )
            charity.save()
            return redirect('user_login-page')
    return render(request, template ,{'title': "Charity_register"})

def charityEditPage(request):
    context = {
            'charity': Charity.objects.get(user=request.user)
        }
    return render(request, 'charity/charityEditPage.html',context)
def deleteProduct(request,PId):
    Product.objects.get(id=PId).delete()
    context = {
        'products': Product.objects.filter(owner=Charity.objects.get(user=request.user)),
        'voluntaryWorks': voluntray_work.objects.filter(owner=Charity.objects.get(user=request.user)),
        'charity': Charity.objects.get(user=request.user)
    }
    return render(request, 'charity/charityProfile.html', context)
def deleteVW(request,VWId):
    voluntray_work.objects.get(id=VWId).delete()
    context = {
        'products': Product.objects.filter(owner=Charity.objects.get(user=request.user)),
        'voluntaryWorks': voluntray_work.objects.filter(owner=Charity.objects.get(user=request.user)),
        'charity': Charity.objects.get(user=request.user)
    }
    return render(request, 'charity/charityProfile.html', context)
def singUpVW(request,VWId):
    user=User.objects.get(username=request.user.username)
    customer=User_Costomer.objects.get(user=user)
    VW=voluntray_work.objects.get(id=VWId)
    VW.volunteers=customer
    VW.save()
    return redirect('home-page')
@login_required
def charityEditSave(request):
    template = 'charity/charityEditPage.html'
    charity = Charity.objects.get(user=request.user)  
    charity.user.name=request.POST.get("EditUsername", "")

    charity.user.description=request.POST.get("EditDescription", "")

    charity.user.fullName=request.POST.get("EditCharityName", "")

    charity.user.email=request.POST.get("EditEmail", "")

    charity.ownerName=request.POST.get("EditOwnerName", "")

    charity.Address=request.POST.get("EditLocation", "")

    charity.user.phoneNumber=request.POST.get("EditPhone", "")

    charity.serialNumber=request.POST.get("EditAcquisitionLicenseCode", "")


    if request.FILES.get('EditprofileImage') is not None and request.FILES.get('EditprofileImage') is not None:
        charity.user.ProfileImage=request.FILES.get("EditprofileImage", "")
    charity.user.save()
    charity.save()
    return redirect('home-page')



@login_required
def addVoluntaryWork(request):
    template = 'charity/newVoluntaryWork.html'
    if request.method == 'POST' and request.FILES.get('voluntaryWorkImage'):
        VWN=request.POST.get("VoluntaryWorkName","")
        VWD=request.POST.get("Description","")
        VWA=request.POST.get("Address","")
        VWI = request.FILES.get("voluntaryWorkImage", "")
        if voluntray_work.objects.filter(name=VWN).exists() and voluntray_work.objects.filter(description=VWD).exists() and voluntray_work.objects.filter(address=VWA).exists():
            return render(request,template, {
                'error_message': 'VoluntaryWork already exists.'
            })
        else:
            image = Image(image=VWI)
            image.save()
            VW = voluntray_work(name=VWN, _type="work", description=VWD, address=VWA, image=image , owner =Charity.objects.get(user=request.user ) )
            VW.save()
            return redirect('voluntary_work-page')
    else:
        return render(request, template)

@login_required
def addProduct(request):
    template = 'charity/newProduct.html'
    if request.method == 'POST':
        PName=request.POST.get("ProductName","")
        PPrice=request.POST.get("Price","")
        PDescription=request.POST.get("Description","")
        PNumbers =request.POST.get("Numbers","")
        PImage= request.FILES.get("productImage", "")
        image = Image(image=PImage)
        image.save()
        P= Product(name=PName ,price=PPrice, description=PDescription, numbers=PNumbers,image=image,owner=Charity.objects.get(user=request.user))
        P.save()
        return redirect('products-page')
    else:
        return render(request, template)


def Charity_profile(request):
    context = {
       'products':Product.objects.filter(owner=Charity.objects.get(user=request.user)),
        'voluntaryWorks':voluntray_work.objects.filter(owner=Charity.objects.get(user=request.user)),
        'charity':Charity.objects.get(user=request.user)
    }
    return render(request, 'charity/charityProfile.html', context)

def Charity_profile_2(request, charity_id):
    charity = Charity.objects.get(id=charity_id)
    user = charity.user
    context = {
       'products':Product.objects.filter(owner=Charity.objects.get(user=user)),
        'voluntaryWorks':voluntray_work.objects.filter(owner=Charity.objects.get(user=user)),
        'charity':charity
    }
    return render(request, 'charity/charityProfile.html', context)

def addVoluntaryWorkPage(request):
    return render(request, 'charity/newVoluntaryWork.html')

def addProductPage(request):
    return render(request, 'charity/newProduct.html')

