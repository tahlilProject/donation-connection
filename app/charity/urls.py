from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.Charity_register, name="charity_register-page"),
    path('profile/', views.Charity_profile, name="Charity_profile_page"),
    path('newProduct/', views.addProductPage, name="newProduct_page"),
    path('newVoluntaryWork/', views.addVoluntaryWorkPage, name="newVoluntaryWork_page"),
    path('addVoluntaryWork/', views.addVoluntaryWork, name="addVoluntaryWork"),
    path('addProduct/', views.addProduct, name="addProduct"),
    path('profile_2/<int:charity_id>/', views.Charity_profile_2, name="Charity_profile_page_2"),
    path('delete/<int:PId>/', views.deleteProduct, name="delete"),
    path('deleteVW/<int:VWId>/', views.deleteVW, name="deleteVW"),
    path('singUpVW/<int:VWId>/', views.singUpVW, name="singUpVW"),
    path('charityEditPage/', views.charityEditPage, name="charityEditPage"),
    path('charityEditSave/', views.charityEditSave, name="charityEditSave")
]