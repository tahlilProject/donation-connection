from django.db import models
from home_page.models import (Product,voluntray_work)
from django.utils.translation import gettext as _
from user.models import User



class Charity(models.Model):
    user = models.OneToOneField( 'user.User' , on_delete=models.CASCADE)
    serialNumber = models.CharField( max_length=50)
    Address = models.CharField( max_length=50 )
    ownerName = models.CharField( max_length=50)
    producs = models.ForeignKey( Product, on_delete=models.DO_NOTHING,blank=True, null=True)
    voluntray_works = models.ForeignKey( voluntray_work , on_delete=models.DO_NOTHING, blank=True, null=True) 
   
    def __str__(self):
        return self.user.fullName
