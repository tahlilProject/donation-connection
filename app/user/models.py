from django.utils import timezone
from django.db import models
from PIL import Image
from django.contrib.auth.models import ( AbstractUser, BaseUserManager)

class User(AbstractUser):
    Is_charity = models.BooleanField (default=False)
    IsUser = models.BooleanField (default=True)
    email=models.EmailField ( max_length=254 )
    fullName =models.CharField ( max_length=50, blank=True, null=True)
    description = models.CharField ( max_length=2000, blank=True, null=True)
    phoneNumber = models.CharField ( max_length=50,blank=True, null=True)
    ProfileImage = models.ImageField (default= 'default.jpg', upload_to='profile_pics')
    #objects = UserManager()
    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        img = Image.open(self.ProfileImage.path)
        output_size = (400, 400)
        img.thumbnail(output_size)
        img.save(self.ProfileImage.path)


class User_Costomer(models.Model):
    user = models.OneToOneField("user.User",on_delete=models.CASCADE)
    NationalNumber = models.CharField(max_length=50,blank=True, null=True)
    permissionForVW = models.BooleanField(default=False)
    

class Cart(models.Model):
    products = models.ForeignKey("home_page.Product", on_delete=models.DO_NOTHING, blank=True, null=True )
    user = models.ForeignKey("user.User_Costomer", on_delete=models.CASCADE)
    price = models.IntegerField()
    date =  models.DateTimeField( default = timezone.now)
    status = models.IntegerField()
    isPayed = models.BooleanField()